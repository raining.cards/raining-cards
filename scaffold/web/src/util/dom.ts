export function animationFrame(): Promise<number> {
  return new Promise((resolve) => window.requestAnimationFrame(resolve));
}
