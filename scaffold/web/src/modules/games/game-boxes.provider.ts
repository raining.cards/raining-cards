import { provide, InjectionKey } from "vue";
import { AsyncProvider } from "@/util/provider";
import { CONFIG } from "@/config";
import { UIGameBoxesState, uiInit } from "@raining.cards/client";

export const GAME_BOXES_INJECT = Symbol(
  "inject::game-boxes"
) as InjectionKey<UIGameBoxesState>;

let state: UIGameBoxesState;

export const gameBoxesProvider: AsyncProvider = async () => {
  const gameBoxesState = await uiInit(CONFIG.apiUrl, CONFIG.wsUrl);

  return () => {
    state = gameBoxesState;
    provide(GAME_BOXES_INJECT, state);
  };
};
