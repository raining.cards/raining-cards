import { onUnmounted, ref, Ref, watch, watchEffect, WatchSource } from "vue";
import { UIGameState, UILobby, UILobbyState } from "@raining.cards/client";
import { useToast } from "@/modules/common/Toast.vue";
import { TYPE } from "vue-toastification";
import { Subscription, unsubscribeAll } from "@raining.cards/util";

export const LOBBY_STATE = ref<UILobbyState>();

export function clearLobbyState() {
  LOBBY_STATE.value?.destroy();
  LOBBY_STATE.value = undefined;
}

export function useLobbyControl() {
  const toast = useToast();

  watchEffect(() => {
    const lobbyState = LOBBY_STATE.value;
    if (lobbyState === undefined) {
      return;
    }
    const subscriptions: Subscription[] = [
      lobbyState.on("error", (it) => toast({ ...it, type: TYPE.ERROR })),
      lobbyState.on("notification", (it) =>
        toast({ ...it, type: (it.type as unknown) as TYPE })
      ),
    ];
    return () => unsubscribeAll(subscriptions);
  });
}

export function useLobby(lobbyState: UILobbyState): Ref<UILobby> {
  const lobby = ref(lobbyState.lobby);
  onUnmounted(lobbyState.on("updated", (it) => (lobby.value = it)));
  return lobby;
}

export function useGameState(
  source: WatchSource<UILobbyState | undefined>
): {
  gameState: Ref<UIGameState | undefined>;
  clearGameState: () => void;
} {
  const gameState = ref<UIGameState>();
  watch(
    source,
    (lobbyState) => {
      if (lobbyState === undefined) {
        gameState.value = undefined;
      } else {
        return lobbyState.on("game", (it) => (gameState.value = it));
      }
    },
    { immediate: true }
  );
  watch(gameState, (_, oldGameState) => oldGameState?.destroy());
  return { gameState, clearGameState };

  function clearGameState() {
    gameState.value = undefined;
  }
}
