import "./dotenv";

import { readJSON, startServer, isEnvironment } from "@raining.cards/server";
import { refreshNotifications, startBroadcastInterval } from "@/notification";
import { GAME_BOXES } from "@/games";
import * as path from "path";

const pkg = readJSON(path.resolve(__dirname, "..", "package.json")) as any;

const ONE_HOUR = 3600000;
const envPort = process.env.RAINING_CARDS_PORT;
const envOrigin = process.env.RAINING_CARDS_ORIGIN;
const environment = process.env.RAINING_CARDS_ENV;

if (environment !== undefined && !isEnvironment(environment)) {
  throw new Error(`Invalid environment: ${environment}`);
}

const { multicast } = startServer({
  environment,
  http: { port: envPort ? parseInt(envPort) : 7582 },
  gameBoxes: GAME_BOXES,
  origin: envOrigin ? new Set([envOrigin]) : undefined,
  meta: {
    homepage: pkg.homepage,
    repository: pkg.repository,
    contributors: pkg.contributors,
  },
});

startBroadcastInterval(multicast, ONE_HOUR, { live: true });

process.on("SIGUSR2", () => {
  console.info("Received SIGUSR2. Refreshing notifications...");
  refreshNotifications(multicast, { live: true }).catch(console.error);
});
