import * as fs from "fs";
import * as path from "path";
import { parse } from "dotenv";

const NO_OVERRIDE = new Set(Object.keys(process.env));
const BASE_DIR = path.join(__dirname, "..");

// process static env files
processFiles(".env", ".env.local");
// static .env files are allowed to alter NODE_ENV (if not within NO_OVERRIDE)
const NODE_ENV = process.env.NODE_ENV ?? "production";
// process environment specific env files
processFiles(`.env.${NODE_ENV}`, `.env.${NODE_ENV}.local`);

function processFiles(...files: string[]) {
  for (const file of files) {
    processFile(file);
  }
}

function processFile(file: string) {
  try {
    const content = fs.readFileSync(path.resolve(BASE_DIR, file));
    const parsed = parse(content);
    for (const key in parsed) {
      if (NO_OVERRIDE.has(key)) {
        continue;
      }
      const value = parsed[key];
      if (value === "") {
        Reflect.deleteProperty(process.env, key);
      } else {
        process.env[key] = value;
      }
    }
  } catch (err) {
    if (err.code !== "ENOENT") {
      throw err;
    }
  }
}
