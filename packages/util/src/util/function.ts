export function identity<T>(it: T): T {
  return it;
}

export interface OnceFunction<Args extends unknown[], T> {
  (...args: Args): T;

  evaluated: boolean;
  result?: T;
}

export function once<Args extends unknown[], T>(
  factory: (...args: Args) => T
): OnceFunction<Args, T> {
  const fn = ((...args: Args) => {
    if (fn.evaluated) {
      return fn.result;
    }
    fn.evaluated = true;
    return (fn.result = factory(...args));
  }) as OnceFunction<Args, T>;
  fn.evaluated = false;
  return fn;
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function onceBy<Key extends object, R, Args extends unknown[]>(
  factory: (key: Key, ...args: Args) => R
): (key: Key, ...args: Args) => R {
  const map = new WeakMap<Key, R>();
  return (key: Key, ...args: Args) => {
    const value = map.get(key);
    if (value !== undefined) {
      return value;
    }
    const result = factory(key, ...args);
    map.set(key, result);
    return result;
  };
}

export function callEach(fns: (() => unknown)[]) {
  for (const fn of fns) {
    fn();
  }
}

export function callIt(fn: () => unknown) {
  fn();
}

export function stub<T>(value: T): () => T {
  return () => value;
}

export const stubTrue = stub(true);
