export function increaseMapValue<T>(
  map: Map<T, number>,
  value: T,
  summand = 1
) {
  map.set(value, (map.get(value) ?? 0) + summand);
}
