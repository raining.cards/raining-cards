export type CountRange =
  | number
  | { min: number; max?: number }
  | { min?: number; max: number };

export interface BakedCountRange {
  min: number;
  max: number;
}

export function bakeCountRange(range?: CountRange): BakedCountRange {
  if (typeof range === "number") {
    return { min: range, max: range };
  } else if (range === undefined) {
    return { min: 0, max: Number.POSITIVE_INFINITY };
  }
  return { min: range.min ?? 0, max: range.max ?? Number.POSITIVE_INFINITY };
}

export function getMax(count: CountRange): number {
  return typeof count === "number"
    ? count
    : (count as { max?: number }).max ?? Number.POSITIVE_INFINITY;
}

export function getMin(count: CountRange): number {
  return typeof count === "number"
    ? count
    : (count as { min?: number }).min ?? 0;
}
