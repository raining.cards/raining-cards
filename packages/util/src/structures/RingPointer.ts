import { assert } from "ts-essentials";

export class RingPointer<T> {
  get items(): T[] {
    return this.list;
  }

  get value(): T {
    return this.current;
  }

  get size(): number {
    return this.list.length;
  }

  private list: T[];
  private current: T;

  constructor(items: T[], private pos = 0) {
    this.list = [...items];
    this.current = this.list[this.pos];
  }

  reset(items: T[], pos = 0) {
    this.list = [...items];
    this.pos = pos;
    this.current = this.list[pos];
  }

  step(step = 1) {
    let pos = this.pos + step;
    const _len = this.list.length;
    while (pos >= _len) {
      pos -= _len;
    }
    while (pos < 0) {
      pos += _len;
    }
    this.pos = pos;
    this.current = this.list[pos];
  }

  pointTo(item: T) {
    this.pos = this.list.indexOf(item);
    assert(this.pos >= 0);
    this.current = item;
  }

  remove(item: T): boolean {
    const idx = this.list.indexOf(item);
    if (idx === -1) {
      return false;
    }
    if (this.pos > idx) {
      this.pos--;
    } else if (this.pos === idx) {
      this.step(-1);
    }
    this.list.splice(idx, 1);
    return true;
  }
}
