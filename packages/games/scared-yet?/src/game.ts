import {
  DefinedGame,
  defineGame,
  GameLogs,
  Participants,
  pushMultiUpdate,
  Transaction,
  TurnOrder,
} from "@raining.cards/server";
import { Dealer } from "@/dealer";
import { Zones } from "@/zones";
import { Actions } from "@/action/actions";
import { GameMeta } from "@/game-meta";
import { Score } from "@/score";
import { Materials } from "@/material/materials";
import { Dungeon } from "@/dungeon";
import { Hero, HeroData } from "@/material/hero-materials";

export interface GameContext {
  participants: Participants;
  turnOrder: TurnOrder;
  localTurnOrder: TurnOrder;
  transaction: Transaction;
  gameLogs: GameLogs;
  materials: Materials;
  zones: Zones;
  actions: Actions;
  gameMeta: GameMeta;
  dealer: Dealer;
  dungeon: Dungeon;
  score: Score;
}

export function useGame(ctx: GameContext): DefinedGame {
  const {
    actions,
    dealer,
    dungeon,
    gameLogs,
    gameMeta,
    materials,
    participants,
    score,
    transaction,
    turnOrder,
    localTurnOrder,
    zones,
  } = ctx;

  return defineGame({
    meta: gameMeta,
    board: zones.board,
    main: async () => {
      while (!score.isConcluded()) {
        dealer.clean();
        dealer.showHeroes();
        const player = turnOrder.current;
        transaction.addPlayerAction(player, actions.chooseHeroGroup);
        gameLogs.add(`${player.name} needs to appoint some hero.`);
        const heroType = (await actions.next()) as Hero;
        const hero = materials.heroes[heroType];
        gameLogs.add(`${hero.face.type.label} is going to face the dungeon.`);
        dealer.deal(hero);
        localTurnOrder.reset(
          turnOrder.players,
          turnOrder.players.indexOf(player)
        );
        await enterDungeon(hero);
      }
      pushMultiUpdate(participants, transaction.close()).catch(console.error);
    },
    closure: score.closure,
    logs: gameLogs,
  });

  async function enterDungeon(heroData: HeroData) {
    while (localTurnOrder.size > 1) {
      await turn();
      localTurnOrder.next();
    }
    turnOrder.current = localTurnOrder.current;
    await dungeon.preDungeon(heroData, ctx);

    const { dungeon: dungeonZone } = zones;
    if (dungeonZone.materials.length > 0) {
      transaction.addPlayerAction(localTurnOrder.current, actions.enterGroup);
      await actions.next();
      await dungeon.fight(heroData, ctx);
      while (dungeonZone.materials.length > 0 && dealer.countHP() > 0) {
        transaction.addPlayerAction(
          localTurnOrder.current,
          actions.continueGroup
        );
        await actions.next();
        await dungeon.fight(heroData, ctx);
      }
      transaction.flush(2000);
    }

    if (dealer.countHP() > 0) {
      score.addDungeonsWon(localTurnOrder.current);
    } else {
      score.addDungeonsLost(localTurnOrder.current);
    }
  }

  async function turn() {
    const player = localTurnOrder.current;
    gameLogs.add(`Next up: ${player.name}`);
    transaction.addPlayerAction(player, actions.drawGroup);
    await actions.next();
  }
}
