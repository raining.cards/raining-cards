import {
  DefinedAction,
  GameLogs,
  idFactory,
  Player,
  Transaction,
  useActions as useCoreActions,
} from "@raining.cards/server";
import { ActionConditions } from "@/action/action-conditions";
import { Materials } from "@/material/material";
import { MaterialFaceGroups } from "@/material/material-face-groups";
import { Zones } from "@/zones";
import { HighestInfo } from "@/state/highest-info";
import { useActionChoose } from "@/action/choose.action";
import { useActionPlayCard } from "@/action/play-card.action";
import { onceBy } from "@raining.cards/util";
import { GameIds } from "@raining.cards/common";
import { Dealer } from "@/dealer";

export type Actions = ReturnType<typeof useActions>;

export function useActions(
  transaction: Transaction,
  actionConditions: ActionConditions,
  dealer: Dealer,
  materials: Materials,
  materialFaceGroups: MaterialFaceGroups,
  zones: Zones,
  gameLogs: GameLogs,
  highestInfo: HighestInfo
) {
  const idf = idFactory<GameIds["actionSchema"]>("actions");
  const choose = useActionChoose(idf, dealer, highestInfo, gameLogs);
  const coreActions = useCoreActions(choose);

  return {
    choose,

    asList: choose as DefinedAction[],
    next,
    of: onceBy(of),
  };

  async function next() {
    const action = await transaction.pushUpdate();
    await coreActions.perform(action);
  }

  function of(player: Player) {
    const playCard = useActionPlayCard(
      player,
      idf,
      actionConditions,
      dealer,
      materials,
      materialFaceGroups,
      zones,
      gameLogs,
      highestInfo
    );
    coreActions.register(playCard);
    const asList: DefinedAction[] = playCard;

    return {
      playCard,
      asList,
    };
  }
}
