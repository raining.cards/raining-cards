import { GameResultGroup, Player, TurnOrder } from "@raining.cards/server";
import { assert } from "ts-essentials";

export type Score = ReturnType<typeof useScore>;

export function useScore(players: Player[], turnOrder: TurnOrder) {
  const score = new Map<Player, number>();

  for (const player of players) {
    score.set(player, 0);
  }

  return { add, access, closure };

  function access(player: Player): number {
    const value = score.get(player);
    assert(value !== undefined, "No score defined for player.");
    return value;
  }

  function add(player: Player, value: number) {
    score.set(player, score.get(player)! + value);
  }

  function closure(): GameResultGroup {
    const stats = turnOrder.players
      .map((player) => ({ player, score: access(player) }))
      .sort((a, b) => b.score - a.score);

    return {
      stats: stats.map(({ player, score }) => ({
        player,
        text: `${score} points`,
      })),
    };
  }
}
