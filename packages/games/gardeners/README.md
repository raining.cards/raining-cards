[Base Repository](https://gitlab.com/xoria/raining-cards/)

# A Raining Cards Game: Gardeners

## Gardeners

- Players: 3 - 5
- Winner: Last player alive
- Related games:
  [Druids](https://www.amigo-spiele.de/spiel/druids)
  ([BGG](https://boardgamegeek.com/boardgame/232417/druids))

A trick-taking game.

Cards:

- 5 different colors with values ranging from 1 to 12 each
- 2x rain
- 2x hedge clippers

Whoever gets five different colors first, loses the round (-3 points).
All other players get the sum of their collected high-cards in points.
After 5 rounds, the player with most points wins the game.

While the rain simply does nothing, the hedge clippers remove the highest color
of the receiving players score zone.
