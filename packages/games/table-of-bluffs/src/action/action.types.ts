import { ToBGame } from "@/ToB.game";
import { ToBGameIds } from "@/ToB.types";
import { ClientAction } from "@raining.cards/server";

export enum ToBActionSchema {
  COUP = "coup",
  INCOME = "income",
  FOREIGN_AID = "foreignAid",

  TAX = "tax",
  ASSASSINATE = "assassinate",
  STEAL = "steal",
  EXCHANGE_ROLES_0 = "exchangeRoles:draw",
  EXCHANGE_ROLES_1 = "exchangeRoles:discard",

  CHANGE_TEAM_SELF = "changeTeam:self",
  CHANGE_TEAM_OTHER = "changeTeam:other",
  COLLECT_TREASURE = "treasury:collect",

  BLOCK_FOREIGN_AID_DUKE = "foreignAidBlock:duke",
  BLOCK_ASSASSINATE_CONTESSA = "assassinateBlock:contessa",
  BLOCK_STEAL_AMBASSADOR = "stealBlock:ambassador",
  BLOCK_STEAL_CAPTAIN = "stealBlock:captain",

  DOUBT = "doubt",
  CONTINUE = "continue",

  FORFEIT_LIFE = "life:forfeit",
}

export enum ToBActionSchemaGroup {
  TURN = "turn",
  EXCHANGE_ROLES_1 = "exchangeRoles:continue",
  DOUBT = "doubt",
  CONTINUE = "continue",
  FORFEIT_LIFE = "forfeitLife",

  BLOCK_FOREIGN_AID = "block:foreignAid",

  RECEIVE_COUP = "receive:coup",
  RECEIVE_ASSASSINATE = "receive:assassinate",
  RECEIVE_STEAL = "receive:steal",
}

export const TOB_ACTION_GROUPS = [
  {
    id: ToBActionSchemaGroup.TURN,
    schemaTypes: [
      ToBActionSchema.COUP,
      ToBActionSchema.INCOME,
      ToBActionSchema.FOREIGN_AID,
      ToBActionSchema.TAX,
      ToBActionSchema.ASSASSINATE,
      ToBActionSchema.STEAL,
      ToBActionSchema.EXCHANGE_ROLES_0,
      ToBActionSchema.CHANGE_TEAM_SELF,
      ToBActionSchema.CHANGE_TEAM_OTHER,
      ToBActionSchema.COLLECT_TREASURE,
    ],
  },

  {
    id: ToBActionSchemaGroup.EXCHANGE_ROLES_1,
    schemaTypes: [ToBActionSchema.EXCHANGE_ROLES_1],
  },
  {
    id: ToBActionSchemaGroup.RECEIVE_COUP,
    schemaTypes: [ToBActionSchema.FORFEIT_LIFE],
  },
  {
    id: ToBActionSchemaGroup.DOUBT,
    schemaTypes: [ToBActionSchema.DOUBT],
  },
  {
    id: ToBActionSchemaGroup.CONTINUE,
    schemaTypes: [ToBActionSchema.CONTINUE],
  },
  {
    id: ToBActionSchemaGroup.FORFEIT_LIFE,
    schemaTypes: [ToBActionSchema.FORFEIT_LIFE],
  },

  {
    id: ToBActionSchemaGroup.BLOCK_FOREIGN_AID,
    schemaTypes: [ToBActionSchema.BLOCK_FOREIGN_AID_DUKE],
  },
  {
    id: ToBActionSchemaGroup.RECEIVE_ASSASSINATE,
    schemaTypes: [
      ToBActionSchema.BLOCK_ASSASSINATE_CONTESSA,
      ToBActionSchema.FORFEIT_LIFE,
    ],
  },
  {
    id: ToBActionSchemaGroup.RECEIVE_STEAL,
    schemaTypes: [
      ToBActionSchema.BLOCK_STEAL_AMBASSADOR,
      ToBActionSchema.BLOCK_STEAL_CAPTAIN,
      ToBActionSchema.CONTINUE,
    ],
  },
];

export type ToBActionHandler = (
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) => Promise<void>;
