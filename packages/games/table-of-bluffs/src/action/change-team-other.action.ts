import { ToBGame } from "@/ToB.game";
import { ToBActionCondition } from "@/condition";
import { ToBZoneType } from "@/zone/zone-types";
import { ToBActionSchema } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import {
  ActionSchema,
  ClientAction,
  ClientActorMaterial,
  last,
} from "@raining.cards/server";
import { ElementType } from "@raining.cards/common";
import { matchAll, matchIt, times } from "@raining.cards/util";
import { PlayerZonesMap } from "@/zone/zones";
import { CREDIT_SYMBOL } from "@/material/face-types";

export function changeTeamOtherSchema(
  game: ToBGame,
  { zonesOthers }: PlayerZonesMap
): ActionSchema<ToBGameIds> {
  return {
    id: ToBActionSchema.CHANGE_TEAM_OTHER,
    name: `Flip (${CREDIT_SYMBOL}2)`,
    description: `Costs: ${CREDIT_SYMBOL}2 (put into treasury). Flip the team of some other player.`,
    origin: {
      type: ElementType.MATERIAL,
      position: zonesOthers[ToBZoneType.PLAYER_TEAM].map((zone) => ({
        zone: zone.id,
      })),
    },
    target: { type: ElementType.GLOBAL },
    condition: matchAll([
      matchIt(ToBActionCondition.CREDITS_MAX_9),
      matchIt(ToBActionCondition.CREDITS_MIN_2),
      matchIt(ToBActionCondition.ALIVE_MIN_3),
    ]),
  };
}

export async function performChangeTeamOther(
  game: ToBGame,
  action: ClientAction<ToBGameIds>
) {
  const material = action.origin as ClientActorMaterial<ToBGameIds>;
  game.transaction.flipGlobal(material);
  game.logs.add(
    `${action.player.name} flips the team of ${
      game.playerByZone.get(material.zone)!.name
    } for ${CREDIT_SYMBOL}2`
  );
  const creditsPosition = last(
    game.zones.byPlayer.get(action.player)![ToBZoneType.PLAYER_CREDITS]
  );
  const treasuryPosition = last(game.zones.global[ToBZoneType.TREASURY]);
  times(2, () => game.transaction.move(creditsPosition, treasuryPosition));
  game.turn.step();
}
