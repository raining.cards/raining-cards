import { conditionsMetaMap } from "@/condition";
import { TOB_ACTION_GROUPS } from "@/action/action.types";
import { ToBGameIds } from "@/ToB.types";
import { PlayerZonesMap, ToBZones } from "@/zone/zones";
import {
  ActionSchema,
  ActionSchemaGroup,
  Client,
  GameMeta,
  Player,
} from "@raining.cards/server";
import {
  groupBy,
  ListAndLookup,
  mapValues,
  pluckId,
  withLookup,
  withoutUnique,
} from "@raining.cards/util";
import { ToBZoneType, ZONE_TYPES } from "@/zone/zone-types";
import { actionSchemas } from "@/action";
import {
  MATERIAL_FACE_TYPE_GROUPS,
  MATERIAL_FACE_TYPES,
} from "@/material/face-types";
import { ToBGame } from "@/ToB.game";

const VOID_META: GameMeta<ToBGameIds> = {
  actionSchemas: withLookup<ActionSchema<ToBGameIds>>([]),
  actionSchemaGroups: [],
  actionConditions: [],

  materialFaceTypes: MATERIAL_FACE_TYPES,
  materialFaceTypeGroups: MATERIAL_FACE_TYPE_GROUPS,

  zoneTypes: ZONE_TYPES,
};

export function createClientMeta(
  game: ToBGame,
  client: Client,
  isPlayer: boolean,
  players: Player[],
  zones: ToBZones
): GameMeta<ToBGameIds> {
  if (!isPlayer) {
    return VOID_META;
  }
  return {
    ...VOID_META,
    ...playerSpecificMeta(game, client as Player, players, zones),
  };
}

function playerSpecificMeta(
  game: ToBGame,
  player: Player,
  players: Player[],
  zones: ToBZones
): Partial<GameMeta<ToBGameIds>> {
  const otherPlayers = withoutUnique(players, player);
  const zonesForPlayer = zonesForPlayerMapping(zones, player, otherPlayers);
  const schemas = withLookup(actionSchemas(game, player, otherPlayers, zones));
  return {
    actionSchemas: schemas,
    actionSchemaGroups: getActionSchemaGroups(game, schemas),
    actionConditions: conditionsMetaMap(zones, zonesForPlayer, otherPlayers),
  };
}

function getActionSchemaGroups(
  game: ToBGame,
  { asLookup }: ListAndLookup<ActionSchema<ToBGameIds>>
): ActionSchemaGroup<ToBGameIds>[] {
  const actionSchemaIdsByType = mapValues(
    groupBy(
      Object.entries(game.actionSchemaMapping).map(([id, type]) => ({
        id,
        type,
      })),
      ({ type }) => type
    ),
    (it) => it.map(pluckId).filter((id) => Reflect.has(asLookup, id))
  );
  return TOB_ACTION_GROUPS.map(({ id, schemaTypes }) => ({
    id,
    schemaIds: schemaTypes.flatMap((type) => actionSchemaIdsByType[type]),
  }));
}

function zonesForPlayerMapping(
  zones: ToBZones,
  player: Player,
  otherPlayers: Player[]
): PlayerZonesMap {
  return {
    ...zones,
    zonesSelf: zones.byPlayer.get(player)!,
    zonesOthers: {
      [ToBZoneType.PLAYER_ROLES]: otherPlayers.map(
        (p) => zones.byPlayer.get(p)![ToBZoneType.PLAYER_ROLES]
      ),
      [ToBZoneType.PLAYER_CREDITS]: otherPlayers.map(
        (p) => zones.byPlayer.get(p)![ToBZoneType.PLAYER_CREDITS]
      ),
      [ToBZoneType.PLAYER_TEAM]: otherPlayers.map(
        (p) => zones.byPlayer.get(p)![ToBZoneType.PLAYER_TEAM]
      ),
    },
  };
}
