# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2021-04-22

### Added

- Fix UI to avoid frequent layout shifts.
- Responsive layout of player zones.

### Fixed

- Do not send new action revisions during answer phase.
- Do not collate visible answers.

## [0.1.0] - 2021-04-15

### Added

- Initial Release
