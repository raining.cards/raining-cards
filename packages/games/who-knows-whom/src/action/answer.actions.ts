import {
  defineAction,
  first,
  IdFactory,
  last,
  Material,
  Player,
  Transaction,
} from "@raining.cards/server";
import { titleCase } from "@raining.cards/util";
import { ElementType, GameIds } from "@raining.cards/common";
import { ANSWERS, Materials } from "@/materials";
import { Zones } from "@/zones";

interface AuthorData {
  player: Player;
  answer: string;
}

const author = new WeakMap<Material, AuthorData>();

export function useAnswerActions(
  idf: IdFactory<GameIds["actionSchema"]>,
  player: Player,
  otherPlayers: Player[],
  transaction: Transaction,
  materials: Materials,
  zones: Zones
) {
  const playerZones = zones.player.asLookup;
  return ANSWERS.flatMap((label) => [
    defineAction(
      {
        id: idf(`answer:${label}:${player.id}:self`),
        name: titleCase(label),
        origin: {
          type: ElementType.ZONE,
          zone: [playerZones[player.id].zone],
        },
        target: { type: ElementType.GLOBAL },
      },
      async () => await placeAnswer({ actor: player, answer: label })
    ),

    defineAction(
      {
        id: idf(`answer:${label}:${player.id}:foreign`),
        name: titleCase(label),
        origin: {
          type: ElementType.ZONE,
          zone: otherPlayers.map((it) => playerZones[it.id].zone),
        },
        target: { type: ElementType.GLOBAL },
      },
      async (action) =>
        await placeAnswer({
          actor: player,
          actee: zones.playerByZone.get(action.origin.zone),
          answer: label,
        })
    ),
  ]);

  async function placeAnswer({
    actor,
    actee,
    answer,
  }: {
    actor: Player;
    actee?: Player;
    answer: string;
  }) {
    const material = (actee === undefined
      ? materials.answersSelf
      : materials.answersForeign)[answer];
    const zone = zones.player.asLookup[(actee ?? actor).id].zone;
    const prevMaterial = zone.materials.find(
      (it) => author.get(it)?.player === actor
    );
    if (prevMaterial !== undefined) {
      transaction.destroy({ zone, material: prevMaterial });
      if (material.is(prevMaterial)) {
        return;
      }
    }
    const nextMaterial = material.factory(
      { label: actee === undefined ? answer : `${actor.name}: ${answer}` },
      { secret: { [actor.id]: 1 } }
    );
    author.set(nextMaterial, { player: actor, answer });
    transaction.create(
      nextMaterial,
      actee === undefined ? first(zone) : last(zone)
    );
  }
}
