export type State = ReturnType<typeof useState>;

export enum StateValue {
  ANSWERS = "answers",
  REVEALED = "revealed",
}

export function useState() {
  return { value: StateValue.ANSWERS };
}
