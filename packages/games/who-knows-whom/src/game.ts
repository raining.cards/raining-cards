import {
  DefinedGame,
  defineGame,
  Lobby,
  pushLobbyParticipants,
  Transaction,
} from "@raining.cards/server";
import { Zones } from "@/zones";
import { Actions } from "@/action/actions";
import { GameMeta } from "@/game-meta";
import { State, StateValue } from "@/state";
import { shuffle } from "@raining.cards/util";
import { sendKeyframes } from "@raining.cards/server/dist/game/game";

export function useGame(
  lobby: Lobby,
  transaction: Transaction,
  gameMeta: GameMeta,
  zones: Zones,
  actions: Actions,
  state: State
): DefinedGame {
  const gameRef = defineGame({
    meta: gameMeta,
    board: zones.board,
    main: async () => {
      // this companion app has no regular end
      // noinspection InfiniteLoopJS
      while (true) {
        await roundFlow();
      }
    },
  });
  return gameRef;

  async function roundFlow() {
    const { participants, creator } = lobby;
    for (const player of participants.players) {
      const playerActions = actions.of(player);
      transaction.addPlayerAction(player, playerActions.answer);
    }
    transaction.addPlayerAction(creator, actions.of(creator).reveal);
    while (state.value === StateValue.ANSWERS) {
      await actions.next();
    }
    transaction.setBaseActions([]);
    transaction.addPlayerAction(creator, actions.of(creator).clear);
    await actions.next();
    // shuffle players and zones
    const players = participants.players;
    shuffle(players);
    pushLobbyParticipants(lobby);
    zones.board.asTree.sort(
      (a, b) =>
        players.indexOf(zones.playerByZone.get(a)!) -
        players.indexOf(zones.playerByZone.get(b)!)
    );
    sendKeyframes(gameRef);
  }
}
