import {
  commonPkgMeta,
  defineGameBox,
  Game,
  pushLobbyParticipants,
  useTransaction,
} from "@raining.cards/server";
import * as pkg from "../package.json";
import { useGameMeta } from "@/game-meta";
import { useGame } from "@/game";
import { useZones } from "@/zones";
import { useActions } from "@/action/actions";
import { shuffle } from "@raining.cards/util";
import { useMaterials } from "@/materials";
import { useState } from "@/state";

const description = `
<p>A simple companion app to party questioning sessions. Any player can choose
yes/no/other for any other players and themselves. When all players are ready,
the answers are revealed (who chose what for whom).</p>`;

export const WHO_KNOWS_WHOM_BOX = defineGameBox(
  "who-knows-whom",
  { players: { min: 2 } },
  { name: "Companion: Who Knows Whom", description, meta: commonPkgMeta(pkg) },
  (lobby): Game => {
    shuffle(lobby.participants.players);
    pushLobbyParticipants(lobby);
    const transaction = useTransaction(lobby.participants, null);
    const zones = useZones(lobby.participants.players);
    const materials = useMaterials();
    const state = useState();
    const actions = useActions(lobby, transaction, materials, zones, state);
    const gameMeta = useGameMeta(materials, zones, actions);

    return useGame(lobby, transaction, gameMeta, zones, actions, state);
  }
);
