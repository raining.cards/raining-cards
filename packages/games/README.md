[Base Repository](https://gitlab.com/xoria/raining-cards/)

# Raining Cards: 1st Party Games

Collection package: `@raining.cards/games`

## Table of Bluffs

Package: `@raining.cards/game--table-of-bluffs`

- Players: 2 - 6
- Winner: Last player alive
- Related games:
  [Coup](https://www.indieboardsandcards.com/index.php/games/coup/)
  ([BGG](https://www.boardgamegeek.com/boardgame/131357/coup)),
  [Coup: Reformation](https://indieboardsandcards.com/index.php/our-games/coup-reformation/)
  ([BGG](https://boardgamegeek.com/boardgame/148931/coup-reformation))

In **Table of Bluffs** each player starts with two roles and two credits. The
roles are secret to the player.
In turn order each player can perform a single action until only one player is
left. Every player can perform any action or reaction of any role, regardless of
him/her owning such a role. Some actions can be reacted to by the other players.
Each role related action can be doubted. If a player has been doubted and does
not own the role for the action he attempted, he permanently loses one role; a
player without role withdraws from the game.
If on the other hand a doubt was not successful, the player who initiated the
doubt loses one role.

In addition, each player is assigned one of two teams. The affiliation can be
changed during the game due to player actions. If two, but not all, players are
in the same team, those players cannot perform any role related actions,
reactions and doubts against each other.

## Gardeners

Package: `@raining.cards/game--gardeners`

- Players: 3 - 5
- Winner: Last player alive
- Related games:
  [Druids](https://www.amigo-spiele.de/spiel/druids)
  ([BGG](https://boardgamegeek.com/boardgame/232417/druids))

A trick-taking game.

Cards:

- 5 different colors with values ranging from 1 to 12 each
- 2x rain
- 2x hedge clippers

Whoever gets five different colors first, loses the round (-3 points).
All other players get the sum of their collected high-cards in points.
After 5 rounds, the player with most points wins the game.

While the rain simply does nothing, ths hedge clippers remove the highest color
of the receiving players score zone.

# Raining Cards: 1st Party Companion Apps

## Who Knows Whom

Package: `@raining.cards/app--who-knows-whom`

- Participants: 2+

A simple companion app to party questioning sessions. Any player can choose
yes/no/other for any other players and themselves. When all players are ready,
the answers are revealed (who chose what for whom).
