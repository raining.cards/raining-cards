export enum ClientCommand {
  LOBBY_CREATE = "lc",
  LOBBY_JOIN = "lj",
  LOBBY_RECONNECT = "lr",
  LOBBY_CHANGE_TEAM = "lt",

  GAME_OPTIONS = "go",
  GAME_START = "gs",
  GAME_ACTION = "gx",
  GAME_ABORT = "ga",
  // GAME_MESSAGE = "gm",
  // GAME_LEAVE = "gl",
}

export const CLIENT_COMMAND_VALUES = Object.values(ClientCommand);
export const CLIENT_COMMAND_SET = new Set(CLIENT_COMMAND_VALUES);
