import {
  ActionSchemaGroupDTO,
  ActionSchemaDTO,
  MaterialFaceTypeGroupDTO,
  MaterialFaceTypeDTO,
  ZoneTypeDTO,
  AnyGameIds,
} from "@/types";
import { CountRange, Condition } from "@raining.cards/util";

export type S2CMessageDTOGameMeta<GI extends AnyGameIds> = GameMetaDTO<GI>;

export interface GameMetaDTO<GI extends AnyGameIds> {
  actionSchemas: ActionSchemaDTO<GI>[];
  actionSchemaGroups: ActionSchemaGroupDTO<GI>[];
  actionConditions: ActionConditionDTO<GI>[];

  materialFaceTypes: MaterialFaceTypeDTO<GI>[];
  materialFaceTypeGroups: MaterialFaceTypeGroupDTO<GI>[];

  zoneTypes: ZoneTypeDTO<GI>[];
}

export interface ActionConditionDTO<GI extends AnyGameIds> {
  id: GI["actionCondition"];
  condition: Condition<ActionConditionItemDTO<GI>>;
}

export interface ActionConditionItemDTO<GI extends AnyGameIds> {
  count: CountRange;
  materialFaceTypeIds?: GI["materialFaceType"][];
  materialFaceTypeGroupIds?: GI["materialFaceTypeGroup"][];
  zoneTypeIds?: GI["zoneType"][];
  zoneIds?: GI["zone"][];
}
