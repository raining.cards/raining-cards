export type S2CMessageDTOActionReject = [
  /* actionIdx */ number,
  /* errorMsg */ string
];
