import { GameBox, GameSchema } from "@/game/game.types";
import { AnyGameIds, GameIds } from "@raining.cards/common";

export type DefinedGameBox<
  GI extends AnyGameIds = GameIds,
  GameProps = unknown
> = GameBox<GI, GameProps>;

export function defineGameBox<
  GI extends AnyGameIds = GameIds,
  GameProps = unknown
>(
  id: string,
  lobby: GameSchema,
  options: Omit<GameBox<GI, GameProps>, "id" | "factory" | "lobby">,
  factory: GameBox<GI, GameProps>["factory"]
): DefinedGameBox<GI, GameProps> {
  return {
    id,
    lobby,
    factory,
    ...options,
  };
}
