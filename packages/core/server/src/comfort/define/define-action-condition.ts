import { IdFactory } from "@/util";
import {
  Condition,
  ConditionData,
  CountRange,
  matchIt,
  pluckId,
  transformCondition,
} from "@raining.cards/util";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import {
  MaterialFaceType,
  MaterialFaceTypeGroup,
} from "@/game/material/material.types";
import { Zone, ZoneType } from "@/game/zone/zone.types";
import {
  ActionCondition,
  ActionConditionItem,
} from "@/game/action/action-condition";

export type DefinedActionCondition<
  GI extends AnyGameIds = GameIds
> = ConditionData<ActionCondition<GI>>;

export function defineActionCondition<GI extends AnyGameIds = GameIds>(
  idf: GI["actionCondition"] | IdFactory<GI["actionCondition"]>,
  condition: Condition<DefineActionConditionItem<GI>>
): DefinedActionCondition<GI> {
  const id = typeof idf === "function" ? idf() : idf;
  return matchIt(defineActionConditionToRaw({ id, condition }));
}

/**
 * Helper function to ensure correct types.
 *
 * @param it The condition item.
 */
export function defineActionConditionItem<GI extends AnyGameIds = GameIds>(
  it: DefineActionConditionItem<GI>
) {
  return matchIt<DefineActionConditionItem<GI>>(it);
}

export interface DefineActionCondition<GI extends AnyGameIds = GameIds> {
  id: GI["actionCondition"];
  condition: Condition<DefineActionConditionItem<GI>>;
}

export function defineActionConditionToRaw<GI extends AnyGameIds>({
  id,
  condition,
}: DefineActionCondition<GI>): ActionCondition<GI> {
  return {
    id,
    condition: transformCondition(condition, defineActionConditionItemToRaw),
  };
}

export interface DefineActionConditionItem<GI extends AnyGameIds> {
  count: CountRange;
  materialFaceTypes?: MaterialFaceType<GI>[];
  materialFaceTypeGroups?: MaterialFaceTypeGroup<GI>[];
  zoneTypes?: ZoneType<GI>[];
  zones?: Zone<GI>[];
}

function defineActionConditionItemToRaw<GI extends AnyGameIds>({
  count,
  materialFaceTypes,
  materialFaceTypeGroups,
  zoneTypes,
  zones,
}: DefineActionConditionItem<GI>): ActionConditionItem<GI> {
  return {
    count,
    materialFaceTypeIds: materialFaceTypes?.map(pluckId),
    materialFaceTypeGroupIds: materialFaceTypeGroups?.map(pluckId),
    zoneTypeIds: zoneTypes?.map(pluckId),
    zoneIds: zones?.map(pluckId),
  };
}
