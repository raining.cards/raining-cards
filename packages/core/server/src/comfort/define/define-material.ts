import { Material, MaterialFace } from "@/game/material/material.types";
import { IdFactory } from "@/util";
import { Reveal } from "@/game/material/reveal.types";
import {
  DefinedMaterialFace,
  MaterialFaceDefinition,
} from "@/comfort/define/define-material-face";
import { AnyGameIds, GameIds } from "@raining.cards/common";
import { assert } from "ts-essentials";

export interface DefinedMaterial<GI extends AnyGameIds = GameIds>
  extends Material<GI> {
  faces: [DefinedMaterialFace<GI>, ...DefinedMaterialFace<GI>[]];
}

export interface MaterialDefinition<GI extends AnyGameIds = GameIds> {
  factory: (
    customMeta?:
      | MaterialFace<GI>["customMeta"] // applied to last face
      | (MaterialFace<GI>["customMeta"] | undefined)[],
    reveal?: Reveal
  ) => DefinedMaterial<GI>;
  is: (it: Material<GI>) => boolean;
}

export interface MaterialSingletonDefinition<GI extends AnyGameIds = GameIds>
  extends MaterialDefinition<GI> {
  instance: DefinedMaterial<GI>;
}

export function defineMaterial<GI extends AnyGameIds = GameIds>(
  idf: GI["material"] | IdFactory<GI["material"]>,
  faceDefs:
    | MaterialFaceDefinition<GI>
    | [MaterialFaceDefinition<GI>, ...MaterialFaceDefinition<GI>[]]
): MaterialDefinition<GI> {
  const instances = new WeakSet<Material<GI>>();
  return {
    factory: (
      customMeta?:
        | MaterialFace<GI>["customMeta"] // applied to last face
        | (MaterialFace<GI>["customMeta"] | undefined)[],
      reveal: Reveal = {}
    ): DefinedMaterial<GI> => {
      const id = typeof idf === "function" ? idf() : idf;
      let faces: [DefinedMaterialFace<GI>, ...DefinedMaterialFace<GI>[]];
      if (Array.isArray(faceDefs)) {
        faces = Array(faceDefs.length) as [
          DefinedMaterialFace<GI>,
          ...DefinedMaterialFace<GI>[]
        ];
        if (Array.isArray(customMeta)) {
          for (let i = 0; i < faceDefs.length; i++) {
            faces[i] = faceDefs[i].factory(customMeta[i]);
          }
        } else {
          for (let i = 0; i < faceDefs.length; i++) {
            faces[i] = faceDefs[i].factory(
              i === faceDefs.length - 1 ? customMeta : undefined
            );
          }
        }
      } else {
        assert(
          !Array.isArray(customMeta),
          "Cannot pass meta array for singular face definition."
        );
        faces = [faceDefs.factory(customMeta)];
      }
      const instance: DefinedMaterial<GI> = {
        id,
        faces,
        reveal,
      };
      instances.add(instance);
      return instance;
    },
    is: (it: Material<GI>) => instances.has(it),
  };
}

export function defineMaterialSingleton<GI extends AnyGameIds = GameIds>(
  idf: GI["material"] | IdFactory<GI["material"]>,
  faceDefs:
    | MaterialFaceDefinition<GI>
    | [MaterialFaceDefinition<GI>, ...MaterialFaceDefinition<GI>[]],
  customMeta?:
    | MaterialFace<GI>["customMeta"] // applied to last face
    | (MaterialFace<GI>["customMeta"] | undefined)[],
  reveal?: Reveal
): MaterialSingletonDefinition<GI> {
  const def = defineMaterial(idf, faceDefs);
  const instance = def.factory(customMeta, reveal);
  return {
    ...def,
    factory: () => instance,
    instance,
  };
}
