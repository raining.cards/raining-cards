import {
  MaterialLocator,
  MaterialPosition,
} from "@/game/material/material.types";
import { accessOrThrow } from "@/game/material/material";
import { debug } from "@/util";
import { Zone } from "@/game/zone/zone.types";
import { AnyGameIds } from "@raining.cards/common";
import { TransactionDirtyState } from "@/comfort/use/transaction/transaction-dirty-state";
import {
  bakeDeltaReveal,
  dropMaterial,
} from "@/comfort/use/transaction/material.util";

export interface TransactionDestroy<GI extends AnyGameIds> {
  destroy: (pos: MaterialLocator<GI>) => void;
  destroyAll: (zone: Zone<GI>) => void;
}

export function useTransactionDestroy<GI extends AnyGameIds>(
  dirtyState: TransactionDirtyState<GI>
): TransactionDestroy<GI> {
  return { destroy, destroyAll };

  function destroy(pos: MaterialLocator<GI>) {
    const { material, position: origin } = accessOrThrow(pos);
    const reveal = bakeDeltaReveal(material, origin);
    debug("Transaction: Destroy", { position: origin, material });
    dropMaterial(origin);
    dirtyState.add(origin, {
      material,
      location: { origin, target: null },
      reveal,
    });
  }

  function destroyAll(zone: Zone<GI>) {
    debug("Transaction: Destroy all", zone);
    for (let i = zone.materials.length - 1; i >= 0; i--) {
      const material = zone.materials[i];
      const position = { zone, index: i } as MaterialPosition<GI>;
      const reveal = bakeDeltaReveal(material, position);
      dirtyState.add(position, {
        material,
        location: { origin: position, target: null },
        reveal,
      });
    }
    zone.materials = [];
  }
}
