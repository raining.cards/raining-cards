import { readFileSync } from "fs";

export function readPkg(packageName: string): unknown {
  return readJSON(require.resolve(`${packageName}/package.json`));
}

export function readPkgVersion(packageName: string): string {
  return (readPkg(packageName) as any).version as string;
}

export function commonPkgMeta(pkg: string | any): any {
  if (typeof pkg === "string") {
    pkg = readPkg(pkg);
  }
  return {
    name: pkg.name,
    version: pkg.version,
    license: pkg.license,
    repository: pkg.repository,
    contributors: pkg.contributors,
  };
}

export function readJSON(file: string): unknown {
  return JSON.parse(readFileSync(file).toString()) as unknown;
}
