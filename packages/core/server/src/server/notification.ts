import { assert, noop } from "ts-essentials";
import {
  S2C_NOTIFICATION_TYPE_SET,
  S2C_NOTIFICATION_TYPES,
  S2CNotificationType,
  ServerCommand,
  SocketMessageLowLevelDTO,
} from "@raining.cards/common";
import * as WebSocket from "ws";
import { s2cMessageToDTO } from "@/api/ws/s2c.serializer";
import { once, Predicate, stubTrue, withoutUnique } from "@raining.cards/util";

export interface Notification {
  date?: Date | null;
  // also exported as NotificationType from @raining.cards/server
  type?: S2CNotificationType;
  timeout?: number | true;
  title?: string;
  message: string;
}

export interface MulticastOptions {
  filter?: Predicate<[WebSocket]>;
  live?: boolean;
}

export interface NotificationData {
  it: Notification;
  filter: Predicate<[WebSocket]>;
  dto: SocketMessageLowLevelDTO;
}

const typesJoined = S2C_NOTIFICATION_TYPES.join('" | "');

export function assertNotification(it: unknown): asserts it is Notification {
  assert(
    typeof it === "object" || it !== null,
    `Expected notification object, got ${typeof it}`
  );
  const { date, type, timeout, title, message } = it as any;
  assert(
    typeof message === "string",
    `Invalid value for Notification#message; expected string, got ${typeof message}`
  );
  assert(
    date == null || date instanceof Date,
    `Invalid value for Notification#date; expected null | undefined | Date, got ${typeof date}`
  );
  assert(
    type === undefined || S2C_NOTIFICATION_TYPE_SET.has(type),
    `Invalid value for Notification#type; expected undefined | "${typesJoined}", got ${typeof type}`
  );
  assert(
    timeout === undefined || typeof timeout === "number" || timeout === true,
    `Invalid value for Notification#timeout; expected undefined | null | true, got ${typeof timeout}`
  );
  assert(
    title === undefined || typeof title === "string",
    `Invalid value for Notification#title; expected undefined | string, got ${typeof title}`
  );
}

export function useMulticast(wsServer: WebSocket.Server) {
  let liveNotifications: NotificationData[] = [];

  wsServer.on("connection", (socket) => {
    for (const { filter, dto } of liveNotifications) {
      if (filter(socket)) {
        socket.send(dto!);
      }
    }
  });

  return { multicast };

  function multicast(
    it: Notification,
    { filter, live }: MulticastOptions = {}
  ) {
    filter = filter ?? stubTrue;
    live = live ?? false;

    const dto = s2cMessageToDTO({
      cmd: ServerCommand.NOTIFICATION,
      date: it.date === undefined ? new Date() : it.date ?? undefined,
      type: it.type ?? S2CNotificationType.INFO,
      title: it.title,
      timeout: it.timeout === true ? undefined : it.timeout ?? false,
      message: it.message,
    });

    for (const socket of wsServer.clients) {
      if (socket.readyState === WebSocket.OPEN && filter(socket)) {
        socket.send(dto);
      }
    }

    if (live) {
      const data = { it, filter, dto };
      liveNotifications.push(data);

      return once(() => {
        liveNotifications = withoutUnique(liveNotifications, data);
      });
    }

    return once(noop);
  }
}
