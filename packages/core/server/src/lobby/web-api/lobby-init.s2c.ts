import { Lobby } from "@/lobby/Lobby";
import {
  AnyGameIds,
  S2CMessageDTOLobbyInit,
  ServerCommand,
} from "@raining.cards/common";
import { lobbyToDTO } from "@/lobby/lobby.dto";
import { Client } from "@/lobby/participant.types";

export interface S2CMessageServerLobbyInit<GI extends AnyGameIds, GameProps> {
  cmd:
    | ServerCommand.LOBBY_CREATED
    | ServerCommand.LOBBY_JOINED
    | ServerCommand.LOBBY_RECONNECTED;
  lobby: Lobby<GI, GameProps>;
  client: Client;
}

export function s2cLobbyInitToDTO<GI extends AnyGameIds, GameProps>({
  lobby,
  client: { id, name, secret },
}: S2CMessageServerLobbyInit<GI, GameProps>): S2CMessageDTOLobbyInit {
  return [lobbyToDTO(lobby), { id, name, secret }];
}
