import { changeTeam } from "@/lobby/Lobby";
import { broadcast } from "@/lobby/communication";
import { C2SMessageServerLobbyChangeTeam } from "@/lobby/web-api/lobby-change-team.c2s";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { AnyGameIds, ServerCommand } from "@raining.cards/common";

export function c2sLobbyChangeTeamHandler<GI extends AnyGameIds>(
  socket: WebSocket,
  { ctx: { lobby, client }, teamIndex }: C2SMessageServerLobbyChangeTeam<GI>
) {
  assert(lobby.game == null, "Game already started.");
  changeTeam(lobby, client, teamIndex);
  broadcast(lobby.participants, { cmd: ServerCommand.LOBBY_TEAMS, lobby });
}
