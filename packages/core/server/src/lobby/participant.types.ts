import * as WebSocket from "ws";
import { PlayerDTO, SpectatorDTO } from "@raining.cards/common";

export interface Participants {
  players: Player[];
  spectators: Spectator[];
  teams?: Player[][];
  all: Client[];
}

export interface Player extends PlayerDTO {
  secret: string;
  socket: WebSocket | null;
}

export interface Spectator extends SpectatorDTO {
  secret: string;
  socket: WebSocket | null;
}

export type Client = Player | Spectator;
