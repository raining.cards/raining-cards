import { Lobby } from "@/lobby/Lobby";
import { participantsToDTO } from "@/lobby/participant";
import { AnyGameIds, LobbyDTO } from "@raining.cards/common";

export function lobbyToDTO<GI extends AnyGameIds, Props>(
  lobby: Lobby<GI, Props>
): LobbyDTO {
  return {
    id: lobby.id,
    creatorId: lobby.creator.id,
    options: lobby.gameProps,
    participants: participantsToDTO(lobby),
    gameBoxId: lobby.gameBox.id,
  };
}
