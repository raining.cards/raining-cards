import { Material } from "@/game/material/material.types";
import { Reveal } from "@/game/material/reveal.types";
import { AnyGameIds, GameIds, UIMeta } from "@raining.cards/common";
import { Player } from "@/lobby/participant.types";

export interface ZoneType<GI extends AnyGameIds> {
  id: GI["zoneType"];
  label?: string;
  description?: string;
  relatedTo?: Player[];
  uiMeta?: UIMeta;
}

export interface Zone<GI extends AnyGameIds = GameIds> {
  id: GI["zone"];
  typeId: GI["zoneType"];
  children?: Zone<GI>[];
  materials: Material<GI>[];
  customMeta?: Partial<Omit<ZoneType<GI>, "id">>;
  reveal: Reveal;
}
