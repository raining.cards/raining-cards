import { Zone } from "@/game/zone/zone.types";
import { Opaque } from "ts-essentials";
import { Reveal } from "@/game/material/reveal.types";
import {
  AnyGameIds,
  GameIds,
  MaterialFaceTypeDTO,
  MaterialFaceTypeGroupDTO,
} from "@raining.cards/common";

export type MaterialFaceTypeGroup<
  GI extends AnyGameIds = GameIds
> = MaterialFaceTypeGroupDTO<GI>;
export type MaterialFaceType<
  GI extends AnyGameIds = GameIds
> = MaterialFaceTypeDTO<GI>;

export interface MaterialFace<GI extends AnyGameIds = GameIds> {
  typeId: GI["materialFaceType"];
  customMeta?: Partial<Omit<MaterialFaceType<GI>, "id">>;
}

export interface Material<GI extends AnyGameIds = GameIds> {
  id: GI["material"];
  faces: [MaterialFace<GI>, ...MaterialFace<GI>[]];
  reveal: Reveal;
}

export interface MaterialWithZone<GI extends AnyGameIds = GameIds> {
  zone: Zone<GI>;
  material: Material<GI>;
}

export type MaterialLocator<GI extends AnyGameIds = GameIds> =
  | MaterialPositionDescriptor<GI>
  | MaterialWithZone<GI>;

/**
 * Describes the position of some material.
 * If the index is negative, it is considered to count from the end of the
 * materials instead of the start. For access operations, -1 addresses the last
 * index. For insert operations, -1 describes a push operation to the end of the
 * materials (`insert-index = materials.length`), while -2 describes the
 * inserted item to result in the second last position for example
 * (`insert-index = materials.length - 1`).
 */
export interface MaterialPositionDescriptor<GI extends AnyGameIds = GameIds> {
  zone: Zone<GI>;
  index: number;
}

/**
 * Describes the position of some material. The type creation helpers usually
 * don't check bounds, so any index outside of `[0,materials.length)` are
 * considered non-existent positions that need to be prohibited/addressed by
 * application logic.
 */
export type MaterialPosition<GI extends AnyGameIds> = Opaque<
  MaterialPositionDescriptor<GI>,
  "descriptor:resolved"
>;

export function safeMaterialPosition<GI extends AnyGameIds>(
  descriptor: MaterialPositionDescriptor<GI>
): MaterialPosition<GI> {
  return descriptor as MaterialPosition<GI>;
}

export interface MaterialWithPosition<GI extends AnyGameIds> {
  material: Material<GI>;
  position: MaterialPosition<GI>;
}

export function isMaterialWithZone<GI extends AnyGameIds>(
  locator: MaterialLocator<GI>
): locator is MaterialWithZone<GI> {
  return Reflect.has(locator, "material");
}
