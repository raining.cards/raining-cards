import { Client, Player } from "@/lobby/participant.types";
import { AnyGameIds, GameIds, GameSchemaDTO } from "@raining.cards/common";
import {
  MaterialFaceType,
  MaterialFaceTypeGroup,
} from "@/game/material/material.types";
import { ListAndLookup } from "@raining.cards/util";
import { Zone, ZoneType } from "@/game/zone/zone.types";
import {
  ActionSchema,
  ActionSchemaGroup,
} from "@/game/action/action-schema.types";
import { Lobby } from "@/lobby/Lobby";
import { ActionCondition } from "@/game/action/action-condition";

/*----------------------------------- Box  -----------------------------------*/

export interface GameBox<GI extends AnyGameIds = GameIds, GameProps = unknown> {
  id: string;
  meta?: unknown;
  name: string;
  description: string;
  lobby: GameSchema;

  factory: (lobby: Lobby<GI, GameProps>, props: GameProps) => Game<GI>;
}

export type GameBoxMap<GI extends AnyGameIds> = Record<
  GameBox<GI>["id"],
  GameBox<GI>
>;

/*----------------------------------- Game -----------------------------------*/

export interface Game<GI extends AnyGameIds = GameIds> {
  meta: GameMeta<GI> | ((client: Client, isPlayer: boolean) => GameMeta<GI>);
  board: GameBoard<GI> | ((client: Client, isPlayer: boolean) => GameBoard<GI>);
  logs?:
    | Iterable<string>
    | ((client: Client, isPlayer: boolean) => Iterable<string>);

  run: () => Promise<GameResultGroup[]>;
}

export interface GameMeta<GI extends AnyGameIds = GameIds> {
  actionSchemas: ListAndLookup<ActionSchema<GI>>;
  actionSchemaGroups: ActionSchemaGroup<GI>[];
  actionConditions: ActionCondition<GI>[];

  materialFaceTypes: MaterialFaceType<GI>[];
  materialFaceTypeGroups: MaterialFaceTypeGroup<GI>[];

  zoneTypes: ZoneType<GI>[];
}

export interface GameBoard<GI extends AnyGameIds>
  extends ListAndLookup<Zone<GI>> {
  asTree: Zone<GI>[];
}

/*---------------------------------- Result ----------------------------------*/

export interface GameResultGroup {
  name?: string;
  stats: GameResultPlayerStats[];
}

export interface GameResultPlayerStats {
  player: Player;
  text?: string;
}

/*---------------------------------- Schema ----------------------------------*/

export type GameSchema = GameSchemaDTO;
