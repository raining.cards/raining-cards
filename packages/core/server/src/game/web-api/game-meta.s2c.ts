import { GameMeta } from "@/game/game.types";
import {
  AnyGameIds,
  S2CMessageDTOGameMeta,
  ServerCommand,
} from "@raining.cards/common";
import { zoneTypeToDTO } from "@/game/zone/zone.dto";
import { actionConditionToDTO } from "@/game/action/action-condition";

export interface S2CMessageServerGameMeta<GI extends AnyGameIds> {
  cmd: ServerCommand.GAME_META;
  meta: GameMeta<GI>;
}

export function s2cGameMetaToDTO<GI extends AnyGameIds>({
  meta,
}: S2CMessageServerGameMeta<GI>): S2CMessageDTOGameMeta<GI> {
  return {
    actionSchemas: meta.actionSchemas.asList,
    actionSchemaGroups: meta.actionSchemaGroups,
    actionConditions: meta.actionConditions.map(actionConditionToDTO),

    materialFaceTypes: meta.materialFaceTypes,
    materialFaceTypeGroups: meta.materialFaceTypeGroups,

    zoneTypes: meta.zoneTypes.map(zoneTypeToDTO),
  };
}
