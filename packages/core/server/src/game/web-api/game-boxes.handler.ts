import { ServerResponse } from "http";
import { HTTP_HEADER_JSON_CONTENT_TYPE } from "@/api/http/common";
import { gameBoxToDTO } from "@/game/game.dto";
import { GameBox } from "@/game/game.types";
import { once } from "@raining.cards/util";
import { ServerOptions } from "@/server/server";

const gameBoxesDTOString = once((gameBoxes: GameBox[]) =>
  JSON.stringify(gameBoxes.map(gameBoxToDTO))
);

export function gameBoxesHandler(
  res: ServerResponse,
  { gameBoxes }: ServerOptions
) {
  res.writeHead(200, HTTP_HEADER_JSON_CONTENT_TYPE);
  res.write(gameBoxesDTOString(gameBoxes));
  res.end();
}
