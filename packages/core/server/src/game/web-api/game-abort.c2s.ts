import { assertContext, ClientContext } from "@/lobby/participant-context";
import * as WebSocket from "ws";
import { assert } from "ts-essentials";
import { AnyGameIds, ClientCommand, GameIds } from "@raining.cards/common";

export interface C2SMessageServerGameAbort<GI extends AnyGameIds> {
  cmd: ClientCommand.GAME_ABORT;
  ctx: ClientContext<GI>;
}

// type C2SMessageDTOGameAbort = never;

export interface C2SMessageClientGameStart {
  cmd: ClientCommand.GAME_ABORT;
}

export function c2sGameAbortFromDTO(
  socket: WebSocket,
  data: unknown // C2SMessageDTOGameAbort
): C2SMessageServerGameAbort<GameIds> {
  assert(data === undefined);
  const ctx = assertContext(socket);
  assert(ctx.lobby.game !== undefined);
  return { cmd: ClientCommand.GAME_ABORT, ctx };
}
