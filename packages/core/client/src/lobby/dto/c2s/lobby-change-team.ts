import { C2SMessageDTOLobbyChangeTeam } from "@raining.cards/common";

export interface C2SMessageClientLobbyChangeTeam {
  teamIndex: number | null;
}

export function c2sLobbyChangeTeamToDTO({
  teamIndex,
}: C2SMessageClientLobbyChangeTeam): string {
  const payload: C2SMessageDTOLobbyChangeTeam = teamIndex;
  return JSON.stringify(payload);
}
