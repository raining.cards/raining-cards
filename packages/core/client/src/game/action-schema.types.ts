import { UIMaterialFaceType, UIMaterialPositionCount } from "@/game/game.types";
import {
  AnyGameIds,
  ElementType,
  ElementTypeSingle,
  GameIds,
} from "@raining.cards/common";
import { Condition } from "@raining.cards/util";
import { UIZone, UIMaterialPosition, UIZoneType } from "@/area/zone.types";

export interface UIActionSchema<
  GI extends AnyGameIds = GameIds,
  Origin extends ElementType = ElementType,
  Target extends ElementTypeSingle = ElementTypeSingle
> extends UIActionSchemaRelation<GI, Origin, Target> {
  id: GI["actionSchema"];
  name: string;
  description?: string;
  check: () => boolean;
}

// Meta type, no runtime use
export interface UIActorTypeMap<GI extends AnyGameIds = GameIds> {
  [ElementType.GLOBAL]: undefined;
  [ElementType.ZONE]: UIZone<GI>;
  [ElementType.MATERIAL]: UIMaterialPosition<GI>;
  [ElementType.MATERIALS]: UIMaterialPosition<GI>[];
}

// Meta type, no runtime use
export interface UIActorSchemaMap<GI extends AnyGameIds = GameIds> {
  [ElementType.GLOBAL]: UIActorSchemaGlobal;
  [ElementType.ZONE]: UIActorSchemaZone<GI>;
  [ElementType.MATERIAL]: UIActorSchemaMaterial<GI>;
  [ElementType.MATERIALS]: UIActorSchemaMaterials<GI>;
}

export interface UIActionSchemaConditions<GI extends AnyGameIds = GameIds> {
  originCondition?: Condition<UIMaterialPositionCount<GI>>;
  targetCondition?: Condition<UIMaterialPositionCount<GI>>;
}

export interface UIActionSchemaRelation<
  GI extends AnyGameIds = GameIds,
  Origin extends ElementType = ElementType,
  Target extends ElementTypeSingle = ElementTypeSingle
> {
  origin: UIActorSchemaMap<GI>[Origin];
  checkOrigin: UIActorTypeMap<GI>[Origin] extends undefined
    ? undefined
    : (origin: UIActorTypeMap<GI>[Origin]) => boolean;

  target: UIActorSchemaMap<GI>[Target];
  checkTarget: UIActorTypeMap<GI>[Target] extends undefined
    ? undefined
    : (target: UIActorTypeMap<GI>[Target]) => boolean;
}

export type UIActorSchemaSingle<GI extends AnyGameIds = GameIds> =
  | UIActorSchemaGlobal
  | UIActorSchemaZone<GI>
  | UIActorSchemaMaterial<GI>;

export type UIActorSchema<GI extends AnyGameIds = GameIds> =
  | UIActorSchemaSingle<GI>
  | UIActorSchemaMaterials<GI>;

export interface UIActorSchemaGlobal {
  type: ElementType.GLOBAL;
}

export interface UIActorSchemaZone<GI extends AnyGameIds = GameIds> {
  type: ElementType.ZONE;
  // restrict zone
  zoneIds: Set<GI["zone"]>;
  zoneType: Set<UIZoneType<GI>>;
}

export interface UIMaterialViewRestriction<GI extends AnyGameIds = GameIds> {
  public?: Set<UIMaterialFaceType<GI>>;
  secret?: Set<UIMaterialFaceType<GI>>;
}

export type UIPositionRestriction<GI extends AnyGameIds = GameIds> =
  | { zone?: never; zoneType: UIZoneType<GI>; index?: number[] }
  | { zone: GI["zone"]; zoneType?: never; index?: number[] };

export interface UIActorSchemaMaterial<GI extends AnyGameIds = GameIds> {
  type: ElementType.MATERIAL;
  // restrict position; negative indices count from top of zone
  position?: UIPositionRestriction<GI>[];
  // restrict material to any of the listed face types
  materialView?: UIMaterialViewRestriction<GI>;
}

export interface UIActorSchemaMaterials<GI extends AnyGameIds = GameIds> {
  type: ElementType.MATERIALS;
  // restrict count
  count: { min: number; max: number };
  // restrict position (e.g. [[A], [B, C]] means either materials within
  // position A or materials within positions B and C.
  position?: UIPositionRestriction<GI>[][];
  // restrict material to any of the listed face types
  materialView?: UIMaterialViewRestriction<GI>;
}
