import { S2CMessageDTOGameLog } from "@raining.cards/common";

export interface S2CMessageClientGameLog {
  time: Date;
  message: string;
}

export function s2cGameLogFromDTO([
  time,
  message,
]: S2CMessageDTOGameLog): S2CMessageClientGameLog {
  return { time: new Date(time), message };
}
