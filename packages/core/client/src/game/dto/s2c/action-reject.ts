import { S2CMessageDTOActionReject } from "@raining.cards/common";

export interface S2CMessageClientActionReject {
  actionIdx: number;
  errorMsg: string;
}

export function s2cGameActionRejectFromDTO([
  actionIdx,
  errorMsg,
]: S2CMessageDTOActionReject): S2CMessageClientActionReject {
  return { actionIdx, errorMsg };
}
