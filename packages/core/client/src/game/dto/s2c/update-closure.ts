import {
  AnyGameIds,
  GameResultGroupDTO,
  GameResultPlayerStatsDTO,
  S2CMessageDTOUpdateClosure,
} from "@raining.cards/common";
import { UIGameState } from "@/game/game.service";
import { UIClient, UIParticipants } from "@/game/game.types";

export interface S2CMessageClientUpdateClosure {
  endTime: Date;
  gameResultGroups: UIGameResultGroup[];
}

export interface UIGameResultGroup {
  name?: string;
  stats: GameResultPlayerStats[];
}

export interface GameResultPlayerStats {
  player: UIClient;
  text?: string;
}

export function s2cGameUpdateClosureFromDTO<GI extends AnyGameIds>(
  { participants }: UIGameState<GI>,
  [endTime, groups]: S2CMessageDTOUpdateClosure
): S2CMessageClientUpdateClosure {
  return {
    endTime: new Date(endTime),
    gameResultGroups: groups.map((it) =>
      gameResultGroupFromDTO(participants, it)
    ),
  };
}

function gameResultGroupFromDTO(
  participants: UIParticipants,
  dto: GameResultGroupDTO
): UIGameResultGroup {
  return {
    name: dto.name,
    stats: dto.stats.map((it) =>
      gameResultPlayerStatsFromDTO(participants, it)
    ),
  };
}

function gameResultPlayerStatsFromDTO(
  { players }: UIParticipants,
  dto: GameResultPlayerStatsDTO
): GameResultPlayerStats {
  return {
    text: dto.text,
    player: players.asLookup[dto.playerId],
  };
}
